package dam.android.MiguelAngel.u3t1initialapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private int count;

    private TextView tvDisplay;
    private Button buttonIncrease, buttonDecrease, buttonReset, buttonIncrease2, buttonDecrease2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();
    }

    private void setUI(){

        tvDisplay = findViewById(R.id.tvDisplay);
        buttonIncrease = findViewById(R.id.buttonIncrease);
        buttonDecrease = findViewById(R.id.buttonDecrease);
        buttonReset = findViewById(R.id.buttonReset);
        buttonIncrease2 = findViewById(R.id.buttonIncrease2);
        buttonDecrease2 = findViewById(R.id.buttonDecrease2);

        buttonIncrease.setOnClickListener(this);
        buttonDecrease.setOnClickListener(this);
        buttonReset.setOnClickListener(this);
        buttonIncrease2.setOnClickListener(this);
        buttonDecrease2.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.buttonIncrease: count++;; break;
            case R.id.buttonDecrease: count--; break;
            case R.id.buttonReset: count = 0; break;
            case R.id.buttonIncrease2: count += 2; break;
            case R.id.buttonDecrease2: count -= 2; break;
        }

        tvDisplay.setText(getString(R.string.number_of_elements) + ": " + count);

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("CONT", count);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        count = savedInstanceState.getInt("CONT");
        tvDisplay.setText(getString(R.string.number_of_elements) + ": " + count);
    }
}